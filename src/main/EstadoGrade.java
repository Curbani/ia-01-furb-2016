package main;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import busca.Antecessor;
import busca.Estado;
import model.DiaEnum;
import model.Disciplina;
import model.Grade;
import model.HorarioEnum;
import model.Professor;
import model.Semestre;

public class EstadoGrade implements Estado, Antecessor {

	private List<Grade> grade = new LinkedList<Grade>();
	public ArrayList<Semestre> semestres = new ArrayList<Semestre>();
	
	// lista de disciplinas pendentes de preenchimento na grade

	public Grade novaGrade = null;
	public int nivel = 0;

	// construtor para etapa inicial
	public EstadoGrade(ArrayList<Semestre> semestre) {
		this.semestres = semestre;
	}

	// construtor para iteração
	public EstadoGrade(ArrayList<Semestre> semestres,  List<Grade> grade, Grade novaGrade, int nivel) {
		this.nivel = nivel;
		for(Semestre semestre : semestres){
			this.semestres.add(new Semestre(semestre));
		}
		
		this.grade = new ArrayList<Grade>(grade);
		this.grade.add(novaGrade);
		this.novaGrade = novaGrade; 
		ajustarDados(); 
	}

	/**
	 * Vai excluir os horarios e as disciplinas que ja estão na grade, 
	 * tal ação é feita para facilitar a definição da meta
	 */
	public void ajustarDados() {
		for(Semestre semest : semestres){
		
			for (Professor prof : semest.getProfessores()) {
				if (prof.equals(novaGrade.getProfessor())) {
					//tira o dia de folga
					if(novaGrade.getHorario() == null && novaGrade.getDisciplina() == null){
						prof.getHorariosDisponiveis().remove(novaGrade.getDia());
						prof.setDiaFolga(novaGrade.getDia());
						return;
					}
					
					if(!prof.getHorariosDisponiveis().isEmpty()){
						// exclui os horarios ja utilizados
						prof.getHorariosDisponiveis().get(novaGrade.getDia()).remove(novaGrade.getHorario());
						if (prof.getHorariosDisponiveis().get(novaGrade.getDia()).size() == 0) {
							prof.getHorariosDisponiveis().remove(novaGrade.getDia());
						}
					}
					if(!prof.getDisciplinas().isEmpty()){
						//atualiza ou exclui as disciplinas que ja esta na grade
						int indexDisciplina = prof.getDisciplinas().indexOf(novaGrade.getDisciplina());
					
						Disciplina disc = prof.getDisciplinas().get(indexDisciplina);
						disc.setAulas(disc.getAulas()-1);
						if(disc.getAulas() == 0){
							prof.getDisciplinas().remove(indexDisciplina);
						}
					}
					break;
				}
			}
		}
	}

	public String toString() {
		StringBuilder str = new StringBuilder();
		if (novaGrade != null) {
			str.append("\n" + novaGrade.getDia());
			if(novaGrade.getHorario() != null){
				str.append( " " + novaGrade.getHorario().getHorario() );
			}
			str.append(": " + novaGrade.getProfessor().getNome());
			if(novaGrade.getDisciplina() != null){
				str.append(", "+novaGrade.getDisciplina().getNome());
			}else{
				str.append(", Tempo para elaborar as aulas");
			}
			
		}
		return str.toString();
	}

	@Override
	public int custo() {
		return 1;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean ehMeta() {
		
		int horariosPendentes = 0;
		int disciplinasPendentes = 0;
		boolean profSemDiaFolga = false;
		
		
		for(Semestre semestre : semestres){
			for (Professor prof : semestre.getProfessores()) {
				for (Map.Entry<DiaEnum, LinkedList<HorarioEnum>> horar : prof.getHorariosDisponiveis().entrySet()) {
					horariosPendentes += horar.getValue().size();
				}
				
				disciplinasPendentes += prof.getDisciplinas().size();
				if(prof.getDiaFolga() == null){
					profSemDiaFolga = true;
				}
			}
		}
		//se nenhum professor tiver horario disponivel
		return (/*horariosPendentes == 0 ||*/ disciplinasPendentes == 0) && !profSemDiaFolga;
	}


	public List<Estado> sucessores() {
		List<Estado> suc = new ArrayList<Estado>();
		
		/**
		 * Disponibilidade	 de	 horários	 dos	 professores:	 Cada	 professor	 possui	 1 dia	
			disponível	para	executar	funções	fora	da	sala	de	aula
		 */
		for(Semestre semestre : semestres){
			for (Professor prof : semestre.getProfessores()) {
				if(prof.getDiaFolga() == null){
					Professor professor = new Professor(prof);
					for (Map.Entry<DiaEnum, LinkedList<HorarioEnum>> horar : professor.getHorariosDisponiveis().entrySet()) {
						DiaEnum dia = horar.getKey();
						
						if(horar.getValue().size() == 2){
							Grade novaGrade = new Grade();
							novaGrade.setProfessor(professor);
							novaGrade.setDia(dia);
				
							if (!grade.contains(novaGrade)) {
								if (validaPossivelGrade(novaGrade) ) {
									suc.add(new EstadoGrade(semestres, grade, novaGrade, this.nivel +1));
								}
							}	
						}
					}
					if(suc.size() > 0){
						return suc;
					}
				}
			}
			
			for (Professor prof : semestre.getProfessores()) {
				Professor professor = new Professor(prof);
				
				for (Disciplina disciplina : professor.getDisciplinas()) {
					disciplina = new Disciplina(disciplina);
					for (Map.Entry<DiaEnum, LinkedList<HorarioEnum>> horar : professor.getHorariosDisponiveis().entrySet()) {
						DiaEnum dia = horar.getKey();
						for (HorarioEnum horario : horar.getValue()) {
							Grade novaGrade = new Grade();
							novaGrade.setProfessor(professor);
							novaGrade.setDisciplina(disciplina);
							novaGrade.setDia(dia);
							novaGrade.setHorario(horario);
	
							if (!grade.contains(novaGrade)) {
								if (validaPossivelGrade(novaGrade)) {
									suc.add(new EstadoGrade(semestres, grade, novaGrade, this.nivel +1));
									//return suc;
								}
							}
						}
					}
					
					if(suc.size() > 0){
						return suc;
					}
				}
			}
		
		}
		return suc;
	}
	
    public List<Estado> antecessores() {
        return sucessores();
    }

	public boolean validaPossivelGrade(Grade novaGrade) {
		
		for (Grade grad : grade) {
			if ((grad.getDisciplina() != null && grad.getDisciplina().equals(novaGrade.getDisciplina())) || 
				grad.getProfessor().equals(novaGrade.getProfessor())) {

				if (grad.getDia().equals(novaGrade.getDia())) {
					
					//tenta jogar o dia de folga, mas ja esta sendo utilizado
					if(grad.getProfessor().equals(novaGrade.getProfessor()) && 
					   novaGrade.getHorario() == null && novaGrade.getDisciplina() == null){
						return false;
					}
					
					// horario incompativel
					if (grad.getHorario().equals(novaGrade.getHorario())) {
						return false;
					}

					// não pode haver aula caixão
					if ((grad.getDisciplina() != null && grad.getDisciplina().equals(novaGrade.getDisciplina()))
							&& grad.getProfessor().equals(novaGrade.getProfessor())) {
						return false;
					}
				}
			}
		}
		return true;
	}

}