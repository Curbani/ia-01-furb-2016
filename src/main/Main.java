package main;

import java.util.ArrayList;
import busca.BuscaIterativo;
import busca.BuscaLargura;
import busca.BuscaProfundidade;
import busca.MostraStatusConsole;
import busca.Nodo;
import model.Semestre;

public class Main {

	public static void main(String args[]) {
		Main main = new Main();
		main.rodar();
	}

	public void rodar() {
		/*
		System.out.println("Teste com um professor");
		buscaProfundidade(CasoTestes.getPopula1ProfessoresSemestre());
		buscaItarativa(CasoTestes.getPopula1ProfessoresSemestre());
		buscaLargura(CasoTestes.getPopula1ProfessoresSemestre());
		System.out.println("Teste com dois professor");
		buscaProfundidade(CasoTestes.getPopula2ProfessoresSemestre());
		buscaItarativa(CasoTestes.getPopula2ProfessoresSemestre());
		buscaLargura(CasoTestes.getPopula2ProfessoresSemestre());
		System.out.println("Teste com tres professor");
		buscaProfundidade(CasoTestes.getPopula3ProfessoresSemestre());
		buscaItarativa(CasoTestes.getPopula3ProfessoresSemestre());
		buscaLargura(CasoTestes.getPopula3ProfessoresSemestre()); */

		buscaLargura(CasoTestes.getPopula2ProfessoresSemestre());
		
	}

	public void buscaItarativa(ArrayList<Semestre> semestres) {
		EstadoGrade inicial = new EstadoGrade(semestres);
		Nodo n = new BuscaIterativo(new MostraStatusConsole()).busca(inicial);
		if (n == null) {
			System.out.println("sem solucao!");
		} else {
			System.out.println("Busca Iterativa - solucao:\n" + n.montaCaminho() + "\n\n");
		}
	}

	public void buscaLargura(ArrayList<Semestre> semestres) {
		EstadoGrade inicial = new EstadoGrade(semestres);
		Nodo n = new BuscaLargura(new MostraStatusConsole()).busca(inicial);
		if (n == null) {
			System.out.println("sem solucao!");
		} else {
			System.out.println("Busca Largura - solucao:\n" + n.montaCaminho() + "\n\n");
		}
	}

	public void buscaProfundidade(ArrayList<Semestre> semestres) {
		EstadoGrade inicial = new EstadoGrade(semestres);
		Nodo n = new BuscaProfundidade(new MostraStatusConsole()).busca(inicial);
		if (n == null) {
			System.out.println("sem solucao!");
		} else {
			System.out.println("Busca Profundidade - solucao:\n" + n.montaCaminho() + "\n\n");
		}
	}
}
