package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import model.DiaEnum;
import model.Disciplina;
import model.HorarioEnum;
import model.Professor;
import model.Semestre;

public class CasoTestes {
	private  static CasoTestes casotestes;
	
	public static CasoTestes getInstance(){
		if(casotestes == null){
			casotestes = new CasoTestes();
		}
		return casotestes;
	}
	
	private CasoTestes(){
		
	}
	public static ArrayList<Semestre> getPopula1ProfessoresSemestre(){
		ArrayList<Professor> professores = new ArrayList<Professor>();

		Disciplina discIA = new Disciplina();
		discIA.setNome("IA");
		discIA.setAulas(1);

		Disciplina discCOMP = new Disciplina();
		discCOMP.setNome("Computacao grafica");
		discCOMP.setAulas(2);
		
		Professor profAndre = new Professor();
		profAndre.setNome("Andre Sestari");
		profAndre.getDisciplinas().add(discIA);
		profAndre.getDisciplinas().add(discCOMP);
		
		profAndre.setHorariosDisponiveis(getHorarios1());
		
		professores.add(profAndre);
		ArrayList<Semestre> semestres = new ArrayList<Semestre>();
		Semestre semestre = new Semestre();
		semestre.setAno(2016);
		semestre.setSemestre(1);
		semestre.setProfessores(professores);
		semestres.add(semestre);
		return semestres;
		
	}
	
	public static ArrayList<Semestre> getPopula2ProfessoresSemestre(){
		ArrayList<Professor> professores = new ArrayList<Professor>();

		Disciplina discIA = new Disciplina();
		discIA.setNome("IA");
		discIA.setAulas(1);

		Disciplina discCOMP = new Disciplina();
		discCOMP.setNome("Computacao grafica");
		discCOMP.setAulas(2);

		Disciplina discGrafos = new Disciplina();
		discGrafos.setNome("Grafos");
		discGrafos.setAulas(1);

		Professor profAndre = new Professor();
		profAndre.setNome("Andre Sestari");
		profAndre.getDisciplinas().add(discIA);
		profAndre.getDisciplinas().add(discCOMP);
		
		Professor profCarlos = new Professor();
		profCarlos.setNome("Carlos Curbani");
		profCarlos.getDisciplinas().add(discGrafos);
		
		profAndre.setHorariosDisponiveis(getHorarios1());
		profCarlos.setHorariosDisponiveis(getHorarios1());
		
		professores.add(profAndre);
		professores.add(profCarlos);
		ArrayList<Semestre> semestres = new ArrayList<Semestre>();
		Semestre semestre = new Semestre();
		semestre.setAno(2016);
		semestre.setSemestre(1);
		semestre.setProfessores(professores);
		semestres.add(semestre);
		
		return semestres;
		
	}
	
	public static ArrayList<Semestre> getPopula3ProfessoresSemestre(){
		ArrayList<Professor> professores = new ArrayList<Professor>();

		Disciplina discIA = new Disciplina();
		discIA.setNome("IA");
		discIA.setAulas(1);

		Disciplina discCOMP = new Disciplina();
		discCOMP.setNome("Computacao grafica");
		discCOMP.setAulas(2);
		
		Disciplina discTeoComp = new Disciplina();
		discTeoComp.setNome("Teoria da Computacao");
		discTeoComp.setAulas(2);

		Disciplina discGrafos = new Disciplina();
		discGrafos.setNome("Grafos");
		discGrafos.setAulas(2);

		Professor profAndre = new Professor();
		profAndre.setNome("Andre Sestari");
		profAndre.getDisciplinas().add(discIA);
		profAndre.getDisciplinas().add(discCOMP);
		
		Professor profCarlos = new Professor();
		profCarlos.setNome("Carlos Curbani");
		profCarlos.getDisciplinas().add(discGrafos);
		
		Professor profMario = new Professor();
		profMario.setNome("Mario da Silva");
		profMario.getDisciplinas().add(discTeoComp);
		
		profAndre.setHorariosDisponiveis(getHorarios1());
		profCarlos.setHorariosDisponiveis(getHorarios1());
		profMario.setHorariosDisponiveis(getHorarios2());
		
		professores.add(profAndre);
		professores.add(profCarlos);
		professores.add(profMario);
		
		
		ArrayList<Semestre> semestres = new ArrayList<Semestre>();
		Semestre semestre = new Semestre();
		semestre.setAno(2016);
		semestre.setSemestre(1);
		semestre.setProfessores(professores);
		semestres.add(semestre);
		return semestres;
		
	}
	
	private static HashMap<DiaEnum, LinkedList<HorarioEnum>> getHorarios1(){
		HashMap<DiaEnum, LinkedList<HorarioEnum>> horarios = new HashMap<DiaEnum, LinkedList<HorarioEnum>>();
		horarios.put(DiaEnum.SEGUNDA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13)));
		horarios.put(DiaEnum.QUINTA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		horarios.put(DiaEnum.SEXTA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		horarios.put(DiaEnum.QUARTA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		return horarios;
	}
	private static HashMap<DiaEnum, LinkedList<HorarioEnum>> getHorarios2(){
		HashMap<DiaEnum, LinkedList<HorarioEnum>> horarios = new HashMap<DiaEnum, LinkedList<HorarioEnum>>();
		horarios.put(DiaEnum.SEGUNDA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		horarios.put(DiaEnum.TERCA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		horarios.put(DiaEnum.QUINTA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		horarios.put(DiaEnum.QUARTA, new LinkedList<HorarioEnum>(Arrays.asList(HorarioEnum.H_12_13, HorarioEnum.H_14_15)));
		return horarios;
	}
}
