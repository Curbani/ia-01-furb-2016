package model;

public enum HorarioEnum {
	H_12_13("13/14"), H_14_15("14/15");
	
	private String horario;

	private HorarioEnum(String horario) {
		this.horario = horario;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}
}
