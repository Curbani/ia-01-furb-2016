package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Professor {

	private String nome;
	private ArrayList<Disciplina> disciplinas;
	private DiaEnum diaFolga;
	private HashMap<DiaEnum, LinkedList<HorarioEnum>> horariosDisponiveis;
	
	public Professor(){}
	
	public Professor(Professor other){
		nome = other.getNome();
		diaFolga = other.diaFolga;
		
		
		disciplinas = new ArrayList<Disciplina>();
		for(Disciplina disciplina : other.disciplinas){
			disciplinas.add(new Disciplina(disciplina));
		}
		
		horariosDisponiveis = new HashMap<DiaEnum, LinkedList<HorarioEnum>>();
		for (Map.Entry<DiaEnum, LinkedList<HorarioEnum>> horar : other.horariosDisponiveis.entrySet()) {
			DiaEnum dia = horar.getKey();
			horariosDisponiveis.put(dia, new LinkedList<HorarioEnum>(horar.getValue()));
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Disciplina> getDisciplinas() {
		if(disciplinas == null){
			disciplinas = new ArrayList<Disciplina>();
		}
		return disciplinas;
	}

	public void setDisciplinas(ArrayList<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public DiaEnum getDiaFolga() {
		return diaFolga;
	}

	public void setDiaFolga(DiaEnum diaFolga) {
		this.diaFolga = diaFolga;
	}

	public HashMap<DiaEnum, LinkedList<HorarioEnum>> getHorariosDisponiveis() {
		if(horariosDisponiveis == null){
			horariosDisponiveis = new HashMap<DiaEnum, LinkedList<HorarioEnum>>();
		}
		return horariosDisponiveis;
	}

	public void setHorariosDisponiveis(HashMap<DiaEnum, LinkedList<HorarioEnum>> horariosDisponiveis) {
		this.horariosDisponiveis = horariosDisponiveis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Professor other = (Professor) obj;

		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		
		return true;
	}
	
}